from django.shortcuts import redirect, render
from booksapp.forms import BookForm
from booksapp.models import Book


# Create your views here.

def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)

def create_book(request):
    context ={}

    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_books")

    context['form'] = form
    return render(request, "books/create.html", context)