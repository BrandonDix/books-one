from django.contrib import admin
from django.urls import path
from booksapp.views import show_books, create_book

urlpatterns = [
    path("create/", create_book, name="create_book"),
    path("", show_books,  name="show_books"),
]
